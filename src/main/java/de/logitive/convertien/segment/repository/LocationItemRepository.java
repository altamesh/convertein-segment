package de.logitive.convertien.segment.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.logitive.convertien.segment.domain.location.LocationItem;

public interface LocationItemRepository extends MongoRepository<LocationItem, String> {


}