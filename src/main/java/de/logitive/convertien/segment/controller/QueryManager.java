package de.logitive.convertien.segment.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.logitive.convertien.segment.domain.shop.Segment;
import de.logitive.convertien.segment.domain.location.LocationItem;
import de.logitive.convertien.segment.domain.shop.*;

@Component
public class QueryManager {
	@Autowired
	LocationManager locationProvider = new LocationManager();

	public List<Segment> findSegments(Shop shop,HttpServletRequest request,Devicetype devicetype) throws JsonProcessingException, IOException {
		List<Segment> segmentList = shop.getSegmentList();
	
		segmentList=segmentList.stream().filter(s->s.isActive()).collect(Collectors.toList());
		if (segmentList.isEmpty()) return segmentList;
		
		if(devicetype!=null) {
			segmentList=segmentList.stream().filter(s->s.getDevicetype()!=null && s.getDevicetype().equals(devicetype)).collect(Collectors.toList());
		}
		if (segmentList.isEmpty()) return segmentList;

		//event
		//TODO: use shop date
		//segmentList=segmentList.stream().filter(s->s.getShopEvent()!=null && s.getShopEvent().getStartdate().before(new Date())
			//	&& s.getShopEvent().getEnddate().after(new Date())).collect(Collectors.toList());
		
		if (segmentList.isEmpty()) return segmentList;
		//TODO: same with day part
		
		LocationItem locationItem = locationProvider.extractLocation(request.getRemoteAddr());
		if(locationItem!=null) {
			segmentList=segmentList.stream().filter(s->s.getLocation()!=null && s.getLocation().getLocationItemList().contains(locationItem)).collect(Collectors.toList());			
		}
		if (segmentList.isEmpty()) return segmentList;
		
		if(locationItem!=null && locationItem.getWeahercondition()!=null) {
			segmentList=segmentList.stream().filter(s->s.getLocation().getLocationItemList().stream().filter(i->i.getWeahercondition()!=null &&  i.getWeahercondition().equals(locationItem.getWeahercondition())).count()>0).collect(Collectors.toList());			
		}
		if (segmentList.isEmpty()) return segmentList;
		
		/*List<DayPart> dayPartList = extraxtDayPart();
		if(!dayPartList.isEmpty()) {
			segmentList=segmentList.stream().filter(s->s.getDayPart()!= null && dayPartList.contains(s.getDayPart())).collect(Collectors.toList());			
		}*/
		return segmentList;
	}

	public List<VirtualCategory> virtualCategory(Shop shop,List<String> fqList) {
		return shop.getVirtualCategoryList().stream().filter(v -> KeyValueHandler.matches(v.getKeyValueList(), fqList))
				.collect(Collectors.toList());
	}

	public Contextable sortItemSet(Shop shop,List<String> fqList) {
		return getContextable(shop,(List<? extends Contextable>) shop.getSortItemSetList(), fqList);
	}

	public Contextable getContextable(Shop shop,List<? extends Contextable> contextableList, List<String> fqList) {

		List<Contextable> contextableWithSegment = contextableList.stream()
				.filter(s -> shop.getSegmentList().contains(s.getSegment())).collect(Collectors.toList());
		
		List<Contextable> contextableWithVirtualCategory = contextableList.stream()
				.filter(s -> virtualCategory(shop,fqList).contains(s.getVirtualCategory())).collect(Collectors.toList());

		List<SortItemSet> contextableWithSegmentAndVirtualCategory = new ArrayList(contextableWithSegment);
		contextableWithSegmentAndVirtualCategory.retainAll(contextableWithVirtualCategory);
		// if both match..super
		if (contextableWithSegmentAndVirtualCategory.isEmpty()) {
			// find the first one with atleast one matching
			Optional<? extends Contextable> contextableOptional = contextableList.stream()
					.filter(s -> (contextableWithSegment.contains(s) || contextableWithVirtualCategory.contains(s)))
					.findFirst();
			if (contextableOptional.isPresent()) {
				return contextableOptional.get();
			} else {
				return contextableList.get(0);
			}
		} else {
			return contextableWithSegmentAndVirtualCategory.get(0);
		}
	}

	public FeedFieldSet feedFieldSet(Shop shop,HttpServletRequest request, List<String> fqList) {
		List<FeedFieldSet> feedFieldSetList = shop.getFeedFieldSetList();
		List<VirtualCategory> virtualCategoryList = virtualCategory(shop,fqList);
		FeedFieldSet feedFieldSet = null;
		for (Segment segment : shop.getSegmentList()) {
			if (feedFieldSet == null) {
				Optional<FeedFieldSet> sortItemSetOptional = feedFieldSetList.stream()
						.filter(s -> s.getSegment().equals(segment)).findFirst();
				if (sortItemSetOptional.isPresent()) {
					feedFieldSet = sortItemSetOptional.get();
				}
			}
		}
		// no segments mathes
		// find the first one without any segment
		if (feedFieldSet == null) {
			Optional<FeedFieldSet> feedFieldSetOptional = feedFieldSetList.stream().filter(s -> s.getName() == null)
					.findFirst();
			if (feedFieldSetOptional.isPresent()) {
				feedFieldSet = feedFieldSetOptional.get();
			}
		}
		return feedFieldSet;
	}
	
	public List<ShopEvent> extractShopevent(Shop shop) {
		List<ShopEvent> shopEventList = shop.getShopEventList();
		shopEventList = shopEventList.stream().filter(e->e.isActive()).collect(Collectors.toList());
		if(shopEventList.isEmpty())return shopEventList;
		return shopEventList;
	}
	
	public List<DayPart> extraxtDayPart(Shop shop) {
		List<DayPart> dayPartList = shop.getDayPartList();
		dayPartList = dayPartList.stream().filter(d->d.isActive()).collect(Collectors.toList());
		if(dayPartList.isEmpty())return dayPartList;
		return dayPartList;
	}
}