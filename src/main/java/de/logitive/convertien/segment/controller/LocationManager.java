package de.logitive.convertien.segment.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.logitive.convertien.segment.domain.location.LocationItem;
import de.logitive.convertien.segment.repository.LocationItemRepository;


@Component
public class LocationManager {
	@Autowired
	private LocationItemRepository locationItemRepository;
	private static final String WEATHER_API="https://api.openweathermap.org/data/2.5/weather?appid=fade05d12bcdcdcaf53df38ce80e7897";
	
	
	public LocationItem extractLocation(String ipString) throws JsonProcessingException, IOException {
		long ipLong = dot2LongIP(ipString);
		System.out.println(locationItemRepository.findAll().get(0));
		Optional<LocationItem> locationItemOptional = locationItemRepository.findAll().stream().filter(l->  l.getIpfrom()<=ipLong && l.getIpto()>=ipLong).findFirst();
		if (locationItemOptional.isPresent()) {
			LocationItem locationItem = locationItemOptional.get();
			updateWeatherConditionIfneeded(locationItem);
			return locationItem;
		} else {
			return null;
		}
	}
	
	private void updateWeatherConditionIfneeded(LocationItem locationItem) throws JsonProcessingException, IOException {
		Calendar now =  Calendar.getInstance();
		now.add(Calendar.MINUTE, -30);
		if(locationItem.getLastWeaherconditionUpdate().before(now.getTime())) {
			String weatherCondition = weatherCondition(locationItem.getLatitute(),locationItem.getLongitute());
			locationItem.setWeahercondition(weatherCondition);
			locationItem.setLastWeaherconditionUpdate(new Date());
		}		
	}
	
	private String weatherCondition(String latitute,String longitute) throws JsonProcessingException, IOException {

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity(WEATHER_API+"&lat="+latitute+"&lon="+longitute, String.class);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree(response.getBody());
		return root.get("weather").get("main").asText();
	}
	
	
	private long dot2LongIP(String ipstring) {
		String[] ipAddressInArray = ipstring.split("\\.");
		long result = 0;
		long ip = 0;
		for (int x = 3; x >= 0; x--) {
			ip = Long.parseLong(ipAddressInArray[3 - x]);
			result |= ip << (x << 3);
		}
		return result;
	}
}
