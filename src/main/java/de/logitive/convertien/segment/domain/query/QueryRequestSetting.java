package de.logitive.convertien.segment.domain.query;

import java.util.List;

import lombok.Data;

@Data
public class QueryRequestSetting {

	//FeedFieldSet feedFieldSet;
	private QueryRequestBoostSetting queryRequestBoostSetting;
	private List<String> synonyms;
	private List<QueryRequestFacetSetting> queryRequestFacetSettingList;
}
