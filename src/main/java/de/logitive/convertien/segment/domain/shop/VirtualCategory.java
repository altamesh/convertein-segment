package de.logitive.convertien.segment.domain.shop;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class VirtualCategory{
	@Id
	private String id;
	private String name;
	private String description;
	private boolean active;
	private List<KeyValue> keyValueList=new ArrayList<KeyValue>();
	private String params;
}
