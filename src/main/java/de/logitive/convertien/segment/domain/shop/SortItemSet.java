package de.logitive.convertien.segment.domain.shop;

import java.util.List;

import lombok.Data;

@Data
public class SortItemSet implements Contextable{
	private String id;
	private String name;
	private boolean active;
	private VirtualCategory virtualCategory;
	private Segment segment;
	private List<SortItem> sortItemList;
}
