package de.logitive.convertien.segment.domain.shop;

import java.util.List;

import lombok.Data;

@Data
public class FeedFieldSet implements Contextable{
	private String id;
	private String name;
	private boolean active;
	private List<FeedField> feedFieldList;
	private VirtualCategory virtualCategory;
	private Segment segment;
}
