package de.logitive.convertien.segment.domain.shop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class KeyValueHandler {

	public static boolean matches(List<KeyValue> keyValueEmbList,List<String> fqList) {
		List<String> afqList = new ArrayList<String>();
		for (KeyValue keyValue : keyValueEmbList) {
			afqList.add(keyValue.getKey() + ":" + keyValue.getValue());
		}
		return fqList.size()==afqList.size()&& afqList.size()>0 && fqList.containsAll(afqList);
		
	}
	
	public static boolean matchesAny(List<KeyValue> keyValueEmbList,Map<String, Object> fieldMap) {
		List<String> fqList = new ArrayList<String>();
		for(Map.Entry entry: fieldMap.entrySet()){
			fqList.add(entry.getKey() + ":" + entry.getValue());
		}
		return matches(keyValueEmbList,fqList);
	}
	public static boolean matches(List<KeyValue> keyValueList,
			Map<String, Object> fieldMap) {

		Map<String, List<String>> kvmap = toMap(keyValueList);
		for (Map.Entry<String, List<String>> entry : kvmap.entrySet()) {
			Object fieldMapObject = fieldMap.get(entry.getKey());
			if (fieldMapObject == null) {
				return false;
			} else if (fieldMapObject instanceof List<?>
					&& !containsAny(entry.getValue(),
							(List<Object>) fieldMapObject)) {
				return false;
			} else if (!(fieldMapObject instanceof List<?>)
					&& !entry.getValue().contains(fieldMapObject)) {
				return false;
			}
		}
		return true;
	}

	private static boolean containsAny(List<String> hostList, List<Object> guestList) {
		for (String string : hostList) {
			if (guestList.contains(string)) {
				return true;
			}
		}
		return false;
	}

	public static Map<String, List<String>> toKyMap(List<String> qList) {
		Map<String, List<String>> keyValueMap = new HashMap<String, List<String>>();
		for (int i = 0; i <= qList.size() - 1; i++) {
			String q = qList.get(i);
			String[] fqArray = q.split(":");
			List<String> list=new ArrayList<String>();
			list.add(fqArray[1]);
			keyValueMap.put(fqArray[0], list);
		}
		return keyValueMap;
	}

	public static Map<String, List<String>> embtoMap(List<KeyValue> keyValueList) {
		return toMap(toKeyValueList(keyValueList));
	}

	public static Map<String, List<String>> toMap(List<KeyValue> keyValueList) {
		Map<String, List<String>> keyValueMap = new HashMap<String, List<String>>();
		for (KeyValue keyValue : keyValueList) {
			List<String> list = (List<String>) keyValueMap.get(keyValue
					.getKey());
			if (list == null) {
				list = new ArrayList<String>();
				keyValueMap.put(keyValue.getKey(), list);
			}
			list.add(keyValue.getValue());
		}
		return keyValueMap;
	}
	
	public static Map<String, List<String>> toMapEmb(List<KeyValue> keyValueList) {
		Map<String, List<String>> keyValueMap = new HashMap<String, List<String>>();
		for (KeyValue keyValue : keyValueList) {
			List<String> list = (List<String>) keyValueMap.get(keyValue
					.getKey());
			if (list == null) {
				list = new ArrayList<String>();
				keyValueMap.put(keyValue.getKey(), list);
			}
			list.add(keyValue.getValue());
		}
		return keyValueMap;
	}

	public static List<KeyValue> toKeyValueList(List<KeyValue> keyValueList) {
		List<KeyValue> akeyValueList = new ArrayList<KeyValue>();
		for (KeyValue keyValueEmb : keyValueList) {
			KeyValue keyValue = new KeyValue();
			keyValue.setId(keyValueEmb.getId());
			keyValue.setKey(keyValueEmb.getKey());
			keyValue.setValue(keyValueEmb.getValue());
			akeyValueList.add(keyValue);
		}
		return akeyValueList;
	}

	public String toQuery(List<KeyValue> keyValueList) {
		return toFilterQuery(toKeyValueList(keyValueList));
	}

	public String toFilterQuery(List<KeyValue> keyValueList) {
		Map<String, List<String>> keyValueMap = toMap(keyValueList);
		return toFilterQuery(keyValueMap);
	}

	public String toFilterQuery(Map<String, List<String>> keyValueMap) {
		StringBuilder sb = new StringBuilder();
		Set<String> keySet = keyValueMap.keySet();
		for (String key : keySet) {
			List<String> valueList = keyValueMap.get(key);
			Collections.sort(valueList);
		}

		List<String> keyList = new ArrayList<String>();
		keyList.addAll(keySet);
		Collections.sort(keyList);
		boolean isFirstOuter = true;
		for (String key : keyList) {
			if (!isFirstOuter) {
				sb.append(" AND ");
			}
			List<String> valueList = keyValueMap.get(key);
			boolean isFirst = true;
			sb.append("(");
			for (String value : valueList) {
				if (!isFirst) {
					sb.append(" OR ");
				}
				sb.append(key);
				sb.append(":");
				sb.append(value);
				isFirst = false;
			}
			sb.append(")");
			isFirstOuter = false;
		}
		return sb.toString();
	}
}
