package de.logitive.convertien.segment.domain.shop;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Segment {
	@Id
	private String id;
	private boolean active;
	private String name;
	private DayPart dayPart;
	private ShopEvent shopEvent;
	private Location location;
	private Devicetype devicetype;
	private String weathercondition;
}
