package de.logitive.convertien.segment.domain.shop;

import lombok.Data;

@Data
public class KeyValue {
	private String id;
	private String key;
	private String value;
}
