package de.logitive.convertien.segment.domain.query;

import lombok.Data;

@Data
public class QueryRequestFacetSetting {

	private String fieldName;
	private Long limit=(long) 1;
	private Long mincount=(long) 100;
}
