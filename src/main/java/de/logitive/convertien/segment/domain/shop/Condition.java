package de.logitive.convertien.segment.domain.shop;

import lombok.Data;


@Data
public class Condition {	
	private String name;
	private String value;

}
