package de.logitive.convertien.segment.domain.shop;

import java.util.List;

import lombok.Data;

@Data
public class QueryAction implements Contextable{
	private String id;
	private List<String> keyWords;
	private boolean active;
	private String navigateTarget;
	private VirtualCategory virtualCategory;
	private Segment segment;
}
