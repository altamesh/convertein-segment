package de.logitive.convertien.segment.domain.shop;

import lombok.Data;

@Data
public class Badge{
	private String id;
	private boolean active;
	private String name;
	private String url;
	private Position position = Position.CENTER;
	private VirtualCategory virtualCategory;
	private ShopEvent shopEvent;
	public enum Position {
		TOP_LEFT, TOP_CENTER, TOP_RIGHT, LEFT, CENTER, RIGHT, BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT,
	};
}
