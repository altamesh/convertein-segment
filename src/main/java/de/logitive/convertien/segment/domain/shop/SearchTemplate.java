package de.logitive.convertien.segment.domain.shop;

import org.springframework.data.annotation.Id;

import lombok.Data;


@Data
public class SearchTemplate implements Contextable{
	@Id
	private String id;
	private boolean active;
	private String name;
	private String text;
	private String css;
	private String templatpagepattern;
	//private String autoCompletetext;
	//private String elementId;
	private VirtualCategory virtualCategory;
	private Segment segment;
	//private String urlpattern;
	private String outputElemetId;
}
