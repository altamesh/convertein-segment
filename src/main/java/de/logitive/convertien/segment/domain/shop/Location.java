package de.logitive.convertien.segment.domain.shop;

import java.util.List;

import lombok.Data;

@Data
public class Location {
	private String id;
	private String name;
	private String description;
	private List<LocationItem> locationItemList;
	private boolean active;	
}
