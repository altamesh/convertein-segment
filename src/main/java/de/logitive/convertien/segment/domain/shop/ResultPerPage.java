package de.logitive.convertien.segment.domain.shop;

import lombok.Data;

@Data
public class ResultPerPage {
	private int pos;
	private String caption;
	private int count;
	
	
}
