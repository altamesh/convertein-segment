package de.logitive.convertien.segment.domain.shop;

import java.util.Date;

import lombok.Data;

@Data
public class DayPart {
	private String id;
	private String name;
	private String description;
	private Date starttime;
	private Date endtime;
	private boolean mon;
	private boolean tue;	
	private boolean wed;
	private boolean thu;
	private boolean fri;	
	private boolean sat;	
	private boolean sun;
	private boolean active;	
}
