package de.logitive.convertien.segment.domain.shop;

import lombok.Data;


@Data
public class Icon {
	private String id;
	private boolean active;
	private String label;
	private String name;
	private String url;
	private Type type;
	public enum Type {
		TEXT_LABEl,IMAGE_LABEL
	}
	
}
