package de.logitive.convertien.segment.domain.shop;

import lombok.Data;

@Data
public class Recommendation {
	private String id;
	private boolean active;
	private String name;
	private Type type = Type.RECNTLY_SEEN;
	private String forurl;
	private VirtualCategory virtualCategory;
	private ShopEvent shopEvent;

	public enum Type {
		RECNTLY_SEEN, RECOMMENTED_FOR_YOU,
	};
	
}
