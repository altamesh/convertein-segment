package de.logitive.convertien.segment.domain.shop;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class Tag {
	private String name;
	private String url;
	private int x;
	private int y;
	private Date lastModified;
	private List<Condition> conditionList; 
	
}