package de.logitive.convertien.segment.domain.query;

import lombok.Data;

@Data
public class QuerySetting {
	private QueryRequestSetting queryRequest;
	private QueryResponseSetting queryResponse;
}
