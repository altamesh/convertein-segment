package de.logitive.convertien.segment.domain.shop;

import java.util.Date;

import lombok.Data;

@Data
public class ShopEvent {
	private String id;
	private String name;
	private String description;
	private Date startdate;
	private Date enddate;
	private boolean active;	
}
