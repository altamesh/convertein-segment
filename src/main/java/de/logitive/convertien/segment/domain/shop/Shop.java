package de.logitive.convertien.segment.domain.shop;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Shop {

	@Id
	private String id;
	//https://global.accessorize.com/en-pk
	//https://global.accessorize.com/de-de/
	private String address;
	//accessorize-pk
	//accessorize-de
	private String name;
	private Language language;
	private LocationItem locationItem;

	private Date lastModified;
	private String stopWords;
	
	//old autocom start
	private int autocompKeypressCount;
	private int autocompWordCount;
	private int autocompItemCount;
	private String autocompSrc;
	private String autocompCss;
	//old autocom end
	//private String clientId;
	private String feedUrl;
	private String solrUrl;
	private String serviceUrl;
	private String feedUsername;
	private String feedPassword;
	private boolean live;
	private String magentoAPIUrl;
	private String magentoAPIKey;
	private String magentoAPISecret;
	private String shopifyAPIUrl;
	private String shopifyAPIKey;
	private String shopifyAPISecret;
	
	
	private List<FeedField> feedFieldList;//depricated... this is fix list
	//private List<SortItem> sortItemList;//depricated
	
	private List<FeedFieldSet> feedFieldSetList;
	private List<SortItemSet> sortItemSetList;
	
	private List<Autocomplete> autocompleteList;//t
	private List<Synonyms> synonymsList;//make list
	private List<SearchTemplate> searchtemplateList; //t	
	private List<ContentTemplate> contentTemplateList; //t	
	private List<Promotion> promotionList;//ranking , BOOST //t
	private List<SponceredItem> sponceredItemList;//t
	private List<QueryAction> queryActionList;//t

	private List<Recommendation> recommendationList;//AI
	//currently not in use
	private List<Tag> tagList;
	private List<ResultPerPage> resultPerPageList;
	private List<Badge> badgeList;
	private List<Icon> iconList;
	
	private List<ShopEvent> shopEventList;
	private List<DayPart> dayPartList;
	private List<VirtualCategory> virtualCategoryList;
	private List<Location> locationList;
	
	private List<Segment> segmentList;
	//DEVICE_TYPE
	//keywordlist??-----
	
	
	
	//facet
	//autu com
	//temp
	//syno
	
	
}