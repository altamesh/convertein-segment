package de.logitive.convertien.segment.domain.location;

public class Weathercondition {

	public static final String CLEAR_SKY = "clear sky";
	public static final String CLOUDS = "clouds";
	public static final String RAIN = "rain";
	public static final String SNOW = "snow";
	public static final String MIST = "mist";
	private String condition;
}
