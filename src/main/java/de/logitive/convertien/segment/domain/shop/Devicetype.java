package de.logitive.convertien.segment.domain.shop;

import lombok.Data;

@Data
public class Devicetype {
	private String id;
	private String name;
}
