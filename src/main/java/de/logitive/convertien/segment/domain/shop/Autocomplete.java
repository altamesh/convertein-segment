package de.logitive.convertien.segment.domain.shop;

import lombok.Data;

@Data
public class Autocomplete implements Contextable{
	private String id;
	private String name;
	private int keypressCount;
	private int wordCount;
	private int itemCount;
	private String src;
	private String css;
	//private String urlpattern;
	private VirtualCategory virtualCategory;
	private Segment segment;
	private String inputElemetId;
}
