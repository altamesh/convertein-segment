package de.logitive.convertien.segment.domain.shop;

import lombok.Data;

@Data
public class Language {
	private String id;
	private String name;
	private String iso;
}
