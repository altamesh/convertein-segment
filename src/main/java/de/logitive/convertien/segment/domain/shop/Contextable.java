package de.logitive.convertien.segment.domain.shop;

public interface Contextable {
	VirtualCategory getVirtualCategory();
	Segment getSegment();
}
