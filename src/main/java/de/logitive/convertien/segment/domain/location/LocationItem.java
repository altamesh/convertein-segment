package de.logitive.convertien.segment.domain.location;

import java.util.Date;

import lombok.Data;

@Data
public class LocationItem {
	private String id;
	private long ipfrom;
	private long ipto;
	private String country;
	private String area;
	private String city;
	private int timeZone;
	private String latitute;
	private String longitute;
	private String weahercondition;
	private Date lastWeaherconditionUpdate;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocationItem other = (LocationItem) obj;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		return true;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		return result;
	}
	
	
}
