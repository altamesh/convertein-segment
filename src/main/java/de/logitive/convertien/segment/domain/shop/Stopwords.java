package de.logitive.convertien.segment.domain.shop;

import java.util.List;

import lombok.Data;

@Data
public class Stopwords {
	private List<String> stopWords;
}
