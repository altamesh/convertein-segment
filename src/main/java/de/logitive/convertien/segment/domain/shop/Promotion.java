package de.logitive.convertien.segment.domain.shop;

import java.util.List;

import lombok.Data;

@Data
public class Promotion implements Contextable{
	private String id;
	public enum Boost {
		SLIGHT(2),MODRATE(3), HEIGH(4),HIGHEST(5);
		private float boost;
		Boost(float boost){
			this.setBoost(boost);
		}
		public float getBoost() {
			return boost;
		}
		public void setBoost(float boost) {
			this.boost = boost;
		}
	};
	private String name;
	private Boost boost;
	private List<String> keyWords;
	private VirtualCategory virtualCategory;
	private Segment segment;
	
}
