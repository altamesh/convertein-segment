package de.logitive.convertien.segment.domain.shop;

import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Synonyms implements Contextable{
	@Id
	private String id;
	private boolean active;
	private List<String> words;
	private String meaning;
	private VirtualCategory virtualCategory;
	private Segment segment;

}
