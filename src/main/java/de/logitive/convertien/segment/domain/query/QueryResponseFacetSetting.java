package de.logitive.convertien.segment.domain.query;

import de.logitive.convertien.segment.domain.shop.FeedField.FacetSortType;
import lombok.Data;

@Data
public class QueryResponseFacetSetting {

	private String fieldName;
	private int facetPos;
	private String facetCaption;
	private FacetSortType facetSortType;
	//private String facetBucket;
	private String multiValueDelimiter;
	private String hierarchieDelimiter;
}
