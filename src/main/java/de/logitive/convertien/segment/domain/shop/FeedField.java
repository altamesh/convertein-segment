package de.logitive.convertien.segment.domain.shop;

import lombok.Data;

@Data
public class FeedField {
	private String id;
	private String name;
	private String lable;
	private Type type;
	private long facetLimit;
	private long facetMincount;
	private String hierarchieDelimiter;
	private String facetCaption;
	//private String facetBucket;
	private String facetCustomSort;
	private boolean isFacet;
	private boolean isSingleSelect;
	private String valueLables;
	private FacetSortType facetSortType = FacetSortType.FREQUENCY_ASC;
	public enum FacetSortType {
		VALUE_ASC, VALUE_DESC, FREQUENCY_ASC, FREQUENCY_DESC, CUSTOM
	}
	public enum Type {
		TEXT, NUMBER
	};
	
}