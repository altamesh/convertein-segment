package de.logitive.convertien.segment.domain.shop;


import lombok.Data;

@Data
public class SortItem {
	private String id;
	private String fieldName;
	private SortType sortType;
	private String lable;
	public enum SortType {
		ASC, DESC
	}
	
}
