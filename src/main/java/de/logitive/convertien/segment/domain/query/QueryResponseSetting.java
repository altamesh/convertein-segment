package de.logitive.convertien.segment.domain.query;

import java.util.List;

import de.logitive.convertien.segment.domain.shop.Segment;
import de.logitive.convertien.segment.domain.shop.SortItem;
import lombok.Data;

@Data
public class QueryResponseSetting {
	private String queryForward;
	private List<QueryResponseFacetSetting> queryResponseFacetList;
	private List<SortItem> sortItemList;
	private List<Segment> segmentList;
}
