package de.logitive.convertien.segment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {
	private final static Logger log = LoggerFactory.getLogger(Application.class);


	public static void main(String[] args) {
		 //RestTemplate restTemplate = new RestTemplate();
		 //Shop shop = restTemplate.getForObject("http://localhost:8080/shops/dcdee572-6977-40fe-95be-ba4b72fec51e?clientKey=logitiveClientKey&apiKey=logitiveapikey1", Shop.class);
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		
	}

}